@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			{{-- Start of accordion --}}

			<div class="accordion" id="accordionExample">

				@foreach($orders as $order)
				<div class="card">
					<div class="card-header" id="headingOne">
						<h2 class="mb-0">
							<button class="btn btn-link w-100" type="button" data-toggle="collapse" data-target="#order-{{ $order->id }}" aria-expanded="true" aria-controls="collapseOne">
								{{ $order->order_number }} / {{$order->created_at->format('F d, Y - h:i:s')}}
								@if(Session::has('updated_transaction') && Session::get('updated_transaction') == $order->id)
									<span class="badge badge-info">
										Status Updated!
									</span>
								@endif

								@if($order->status->name == "pending")
									<span class="badge badge-warning float-right"> {{$order->status->name}} </span>
								@elseif($order->status->name == "completed")
									<span class="badge badge-success float-right"> {{$order->status->name}} </span>
								@endif
							</button>
						</h2>
					</div>

					<div id="order-{{ $order->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							{{-- start of card body --}}

							<h5 class="card-title text-center">Summary</h5>
							<div class="table-responsive mb-4">
								{{-- Start of table --}}

								<table class="table table-sm table-bordered">
									<tbody>
										<tr>
											<td>Costumer Name: </td>
											<td><strong>{{$order->user->name}}</strong></td>
										</tr>

										<tr>
											<td>order Number: </td>
											<td><strong>{{ strtoupper($order->order_number) }}</strong></td>
										</tr>

										<tr>
											<td>Mode of Payment: </td>
											<td>{{$order->payment_mode->name}}</td>
										</tr>

										<tr>
											<td>Status</td>
											<td>
												{{$order->status->name}}
												<form action="{{ route('order.update', ['order' => $order->id ]) }}" method="post" class="p-3 bg-secondary rounded">
													@csrf
													@method('PUT')
													<label for="edit-order-status">Change Status</label>
													<select class="custom-select mb-1" id="edit-order-{{ $order->id }}" name="status">
														
														@foreach($statuses as $status)


														<option value="{{ $status->id }}"

															{{ $status->id == $order->status->id ? "selected": ""}}>

															{{ $status->name }}</option>
															@endforeach
														</select>
														<button class="btn btn-primary"> Change Status</button>
													</form>
												</td>
											</tr>

											<tr>
												<td>Date</td>
												<td>{{$order->created_at->format('F d, Y')}}</td>
											</tr>

											<tr>
												<td>Total</td>
												<td>&#8369; {{number_format($order->total,2)}}</td>
											</tr>
										</tbody>

										<tfoot>
											<tr>
												<td colspan="2">
													<a href="{{ route('order.show', ['order' => $order->id])}}" class="btn btn-primary">View Details</a>
												</td>
											</tr>
										</tfoot>

									</table>{{-- end of table --}}


								</div>{{-- end of card body --}}


							</div>
						</div>
					</div> {{-- end of card --}}
					@endforeach
				</div>

				{{-- End of accordion --}}
			</div>
		</div>
	</div>

	@endsection