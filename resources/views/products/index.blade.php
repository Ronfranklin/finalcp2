@extends('layouts.app')

@section('content')

@if(Session::has('destroy_message'))
<div class="alert alert-success">
	{{ Session::get('destroy_message') }}	
</div>
@endif
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<div class="jumbotron">
				<h1 class="text-center">Online Storage</h1>
			</div>
		</div>
	</div>



	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			<div class="row">

				{{-- @if($products->category_id === 1) --}}
				@foreach($products as $product)
				<div class="col-12 col-md-4">
					{{-- {{dd($product)}} --}}
					{{-- <h2 class="text-center">{{$product->category->name}}</h2> --}}
					<div class="card ">
						<img src="{{ url('/public/' .$product->image) }}" class="card-img-top cardImg">
						<div class="card-body cardImg">
							
							<h2 class="card-title">{{ $product->name }}</h2>
							<p class="card-text">
								<strong>
								Stock: {{number_format( $product->stock) }}
								</strong>
							</p>

							<p class="card-text">
								{{ $product->description}}
							</p>

						</div>
					</div>
					
					<div class="card-footer">
						{{-- Add to cart button --}}

						<form action="{{ route('cart.store') }}" method="POST" class="add-to-cart-field">
							@csrf

							<div class="form-group">

								<input type="hidden" name="id" value="{{$product->id}}">
								<label for="quantity">Quantity</label>
								<div class="input-group mb-3">
									
									<div class="input-group-prepend">
										<button class="btn btn-outline-secondary deduct-quantity" data-id= "{{$product->id}}" type="button" >-</button>
									</div>
									
									<input 
									type="text"
									value="1" 
									name="stock" 
									class="form-control input-quantity" 
									placeholder="Enter Quantity" 
									data-id= "{{$product->id}}"
									>
									
									<div class="input-group-append">
										<button class="btn btn-outline-secondary add-quantity" data-id= "{{$product->id}}" type="button">+</button>
									</div>
								</div>
							</div>
							<button class="btn btn-outline-success my-1 w-100 ">Add to cart</button>
						</form>
												<a
						class="btn btn-block btn-outline-dark my-1" 
						href="{{ route('products.show', ['product' => $product->id]) }}">
						View Product
					</a>
					
					<a 
					href="{{ route('products.edit', ['product' => $product->id]) }}" 
					class="btn btn-outline-info w-100 my-1">Edit Product
				</a>

				<form action="{{ route('products.destroy', ['product' => $product->id])}}" method="post">
					@csrf
					@method('DELETE')

					<button {{ route('products.destroy', ['product' => $product->id]) }}" class="btn btn-outline-danger w-100  my-1"> Delete Product
					</button>

				</form>
			</div>

		</div>
		@endforeach
		{{-- @endif --}}
	</div>
</div>
</div>
</div>

@endsection