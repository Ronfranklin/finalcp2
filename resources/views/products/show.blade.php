@extends('layouts.app')

@section('content')



<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center">View This Awesome Product</h3>
			<hr>

			<div class="card">
				<img src="{{ url('/public/' .$product->image) }}" class="card-img-top">
				<div class="card-body">
					
					<h2 class="card-title">{{ $product->name }}</h2>
					<p class="card-text">
						<strong>
							Stock:  {{ $product->stock}}
						</strong>
					</p>
					<p class="card-text">
						{{ $product->description}}
					</p>

				</div>
			</div>

			<div class="card-footer">
				{{-- Add to cart button --}}
				<a href="" class="btn btn-outline-success text-center">Add Product</a>

				<a 
				class="btn btn-block btn-outline-dark my-1" 
				href="{{ route('products.show', ['product' => $product->id]) }}">
				View Product
			</a>
		
			<a 
			href="{{ route('products.edit', ['product' => $product->id]) }}" 
			class="btn btn-outline-info w-100 my-1">Edit Product
		</a>

		<form action="{{ route('products.destroy', ['product' => $product->id])}}" method="post">
			@csrf
			@method('DELETE')
			<button {{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-outline-danger w-100  my-1">Delete Product</button>

		</form>
	</div>
</div>
</div>
</div>

@endsection

