<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function product()
	{
		return $this->belongstoMany('App\Product');
	}

	public function status()
	{
		return $this->belongsTo('App\Status');
	}
}
