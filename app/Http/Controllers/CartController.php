<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::has('order')){
            // get all keys of session
            $product_ids= array_keys(Session::get('order'));

            // we use the ids to query to the database and select only needed products
            $products = Product::find($product_ids);

            foreach ($products as $product) {
                // add the product quantity as an attribute
                $product['stock'] = Session::get("order.$product->id");

            }
            return view('orders.index')->with('products', $products);
        }

        return view('orders.index');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'id' => 'required',
        'stock' => 'required|numeric|min:1|max:99'
    ]);

       $product_quantity = $request->input('stock');
       $id = $request->input('id');

        // set entry to be pass to session
        // $add_to_cart = [$id => $product_quantity]; 

        // store to session
       $request->session()->put("order.$id", $product_quantity);

       return redirect(route('cart.index'));
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Session::forget("order.$id");
        return redirect( route('cart.index'));
    }

    public function clear()
    {
        Session::forget("order");
        return redirect( route('cart.index'));
    }
}
