<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Session;
use Str;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $orders = Order::all();
        $statuses = Status::all();
        return view('transactions.index')
        ->with('orders', $orders)
        ->with('statuses,', $statuses);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Flow
            // 1. create an entry in transaction table with the following attribute
                    // transation_number -> unique
                    // user_id_id
                    // payment_mode_id
                    // status_id
                // 2. add an entry/entries transation_product - linked to the newly created entry in transation table
            // 3. compute the total price 
            // 4. update the "total" attibute of the newly created entry in transaction
            // 5. clear cart session

        // ===============================
        // 1. create an entry in transaction table with the following attribute
                    // transation_number -> unique
                    // user_id_id
                    // payment_mode_id
                    // status_id

        $order = new Order;
        // $user_id = Auth::user()->id;
        $branch = Auth::user()->branch_id;
            // transation_number -> unique
            // format = randomcharacter+userid+time
        $order_number = Str::random(15) . time() . "_" .Auth::user()->id;
                    // user_id
        $user_id = Auth::user()->id;
                    // payment_mode_id
                        // default
                    // status_id
                        // default
        $order->order_number = $order_number;
        $order->user_id = $user_id;
        $order->branch_id = $branch;
        $order->save();
            // ===============================
        // 2. add an entry/entries transation_product - linked to the newly created entry in transation table
            // 2.1 get the keys from the session; this contains the product the user want to buy
        // $order_ids = array_keys(Session::get('order'));
        //     // 2.2 use the array of product id find it in database
        // $products = Product::find($order_ids);
            // 2.3 for every products listed in cart add entry to the pivot table transaction product and include data(subtotal,quantity,price, product id,transaction id)

            // dd(Session::get('cart'));
            // dd($products);

        // $transaction->products()->attach(id of referenced table)
        // $product->transactions()->attach(id of transaction)
        // foreach (Session::get('order') as $order_id => $stock) {
        //     foreach ($products as $product) {
        //         if($product->id == $order_id){
        //             $order->products()->attach(
        //                 $product->id, 
        //                 [
        //                     'stock' => $stock,
        //                 ]
        //             );
        //         }
        //     }
        // }

        // $transaction->products()->attach(4, ['quantity' => 100, 'price' => 500, 'subtotal' => 50000] );
            // ====================================
              // 3. compute the total price

        foreach ($order_product as $order_product) {
            $total += $transaction_product->pivot->subtotal;
        // }

        // ====================================
            // 4. update the "total" attibute of the newly created entry in transaction
        // $transaction->total = $total;
        // $transaction->save();
        // ====================================
        // 5. clear cart session
        Session::forget('cart');

        return redirect( route('order.index', ['transaction' => $order->id]) );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
         return view('order.show')->with('orders',$orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        Session::forget("orders.$id");
        return redirect( route('order.index'));
    }

    
}
