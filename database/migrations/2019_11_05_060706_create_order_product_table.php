<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            //quantity
            $table->bigInteger('quantity');
            
            $table->unsignedBigInteger('branch_id')->nullable(false);
            $table->foreign('branch_id')
            ->references('id')
            ->on('branches')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            // $table->decimal('total',8,2);
            $table->unsignedBigInteger('status_id')->nullable(false);
            $table->foreign('status_id')
            ->references('id')
            ->on('statuses')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            
            $table->unsignedBigInteger('order_id')->nullable(false);
            $table->foreign('order_id')
            ->references('id')
            ->on('orders')
            ->onDelete('restrict')
            ->onUpdate('cascade');


            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
